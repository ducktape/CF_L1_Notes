# What is CrossFit?
- Constantly varied
	- Broad, general and inclusive
	- Variation in Load: change in output Force
	- Variation in Reps/Rounds: change in Distance
	- Variation in Duration: change in Time
	- Variation in Movements: Change in Power
	- Variation in Environments
- Functional movements
	- Natural
		- Corrolaries can be found in day to day
	- Universal Motor Recruitment Pattern
		- Cascade of firing, think of picking something up from the ground
	- Essential
		- You can't really live without them (squats = standing up)
	- Safe
	- Compound, yet irreducible (as a system)
	- Follow the core to extremety pattern
	- Allow for moving a large load over a long distance in a short time
	- $$\frac{(Force*Distance)}{Time}=\frac{Work}{Time}=Power$$
- High intensity
	- Average Power output = Intensity
	- Intensity is key for creating adaptation and results
	
These three components work together to to allow CrossFit to build *Capacity for the unknown and the unknowable*, aka General Physical Preparedness (GPP)

# What is Fitness?
There are 4 models used to view / describe fitness
## 10 General Physical Skills
**Organic**
1. Cardiovasuclar/Respiratory Endurance
2. Strength
3. Stamina
4. Flexibility

**Both**
5. Power
6. Speed

**Neurological**
7. Coordination
8. Agility
9. Balance
10. Accuracy

**Organic**: Cellular level adaptations, gained via training

**Neurological**: Neurological level adaptations, gained via practice (skills)

**Both**: Organic readiness + Neurological ability

Key Point: **Balance of physiological adaptation**

## Hopper Model
Your average performance across varied movements, time, and loads. 

Key Point: **Balance of skills and drills**

## Metabolic Pathways
1. Oxidative Pathway (Anaerobic)
	- 40 % effort
	- Greater than 120 seconds long
2. Phosphagen / Phosphocreatine (Anerobic)
	- 100 % effort (1 RM type stuff)
	- About 10seconds
3. Glycolitic (Anaerobic)
	- 70 % Effort
	- up to 120 seconds
	
Key point: **Balance of bio-energetics**

## Sickness, Wellness, Fitness

- Health is a continum from sicness -> fitness. 
- Aim to gain fitness as a hedge against sickness.

Key Point: **Balance of lifestyle**

## Summary

- **Fitness**: Work capacity across broad time and modal domains.
- **Work Capacity**: The area under the curve of Power / Time
- **Health**: Fitness Throughout your life
- CrossFit aims to make athletes good at everything, therefore trains the glycolitic a little harder than the other pathways, and is not an expert in any one disipline. At the core though the athlete must remain healthy. 
- The needs of athletes differ by degree, not by kind
- Train your weaknesses
- Hierarchy of development of the athlete
	- Nutrition -> Metabolic Conditioning -> Gymnastics -> Weightlifting/Throwing -> Sport

# Technique

**Fitness**: work capacity across broad time and modal domains.

**Three Components of a Good Fitness Prgram**

1. Safety
	- What is the risk of injury
2. Efficacy
	- What adaptation is produced
	- We want increased work capacity as the gold standard
3. Efficiency
	- Rate that the adaption is produced

**Technique**: maximizes the Work Accompished for the Energy Expended

**Strength**: Productive application of force

**Virtuosity**: Doing the common uncommonly well

**Three Part Charter**

1. Mechanics - learn the basics
2. Consistency - able to reproducibly perform moments
3. Intensity - find the technique / intensity balance

The key takeaway here ist hat we are trying to balance Safety, Efficacy, and Efficiency to produce results. To do this we end up balancing Technique and Intensity in order to produce the best reuslts. This is done with Threshold Training. Ind doing all this we hold to the **Three Part Charter** to ensure balance is kept.

# Nine Foundational Movements
**Repeated key concepts**
1. Midline Stability
2. Posterior Chain Recruitment
3. Range of Motion
4. Core to Extremity
5. Acitve Shoulders

## Squat
### Air Squat
**Points of Performance**
1. Hips Descend back and down (Core to Extremity)
2. Lumbar curve maintained (Midline stability)
3. Knees in line with toes
4. Hips descend lower than knees (Range of Motion)
5. Heels down (Posterior Chain Recruitment)

### Front Squat
**Points of Performance**
1. All points from Air Squat Carry over
2. Maintain frontrack position
3. Bar in the 'Frontal Plane' or close to it

### Overhead Squat
**Points of Performance**
1. All points from Air Squat Carry over
2. Active shoulder Position driving upward (Active Shoulder)
3. Bar in Frontal plane through whole movement

## Press
### Shoulder Press "Press"
**Points of Performance**
1. Spine neutral and legs extended (Midline Stability)
2. Heels down (Posterior Chain Recruitment)
3. Bar moves over the middle of the foot
4. Shoulder pus up into the bar (Active Shoulder)
5. Elbows locked out at the top (Range of motion)

### Push Press "Dip-Drive-Press"
**Points of Performance**
1. All points from Shoulder Press
2. Verticle Torso as hips and knes flex in dip (Midline Stability)
3. Hips and Legs extend, then arms press (timing)
4. Heels down until hips and knees extend (Posterior Chain Recruitment)
5. Bar moves over middle of the foot
6. Complet lock out at the top (Range of motion)

### Push Jerk "Dip-Drive-Press Under-Stand"
**Points of Performance**
1. All points of Press and Push Press
2. Hips and knees extend rapidly, then arms press to drive under the bar

## Deadlift
### Deadlift
**Points of Performance**
1. Lumbar Curve Maintained (Midline Stability)
2. Hips and shoulder rise at the same rate until the bar passes the knee
3. Hips then open
4. Bar moves ove the middle of the foot
5. Heels down

### Sumo Deadlift High Pull
**Points of Performance**
1. Lumbar curve maintained
2. Hips and shoulders rise at the same rate until bar passes the knee
3. Hips then extend rapidly
4. Heels down until hips and legs extend
5. Shoulders shrug, then arms pull
6. Elbows high and outside
7. Bar moves over the middle of the foot

### Medicine-Ball Clean
**Points of Performance**
1. Lumbar curve maintained
2. Hips extend rapidly
3. Shoulders then shrug
4. Heels down until the hips and kenes extend
5. Arms then pull under the bottom of the squat
6. Ball stays close to body